export default {
    core:{
        auth: {
            register: '/register/',
            login: '/login',
            logout: '/logout',
            guardians: {
                login: '/guardian/login',
                logout: 'guardian/logout',
            },
            students: {
                login: '/student/login',
                logout: 'student/logout',
            }
        },
        users: {
            root: '/api/users/',
            self: '/self',
            withId: function (id) {
                return '/api/users/' + id;
            }
        },
        students: {
            root: '/api/students/',
            self: '/self',
            withId: function (id) {
                return '/api/students/' + id;
            }
        },
        subjects: {
            root: '/api/subjects/',
            withId: function (id) {
                return '/api/subjects/' + id;
            }
        },
        assignments: {
            root: '/api/assignments/',
            self: '/self'
        },
        attendances: {
            root: '/api/attendances/',
            withId: function (id) {
                return '/api/attendances/' + id;
            }
        },
        guardians: {
            root: '/api/guardians/',
            self: '/self',
            withId: function (id) {
                return '/api/guardians/' + id;
            }
        },
        events: {
            root: '/api/events/',
            self: '/self',
        },
        grades: {
            root: '/api/grades/',
            self: '/self',
        },

        behaviours: {
            root: '/api/behaviours/',
            self: '/self',
        },

        exams: {
            root: '/api/exams/',
            self: '/self',
        }
    }
}
