<?php

namespace App;

use App\AbstractModel as Model;

class Attendance extends Model
{
    protected $fillable = [
        'subject_id',
        'student_id',
        'register',
      ];

      public function students() {
        return $this->belongsToMany(Student::class)->withPivot('student_id', 'subject_id', 'attendance_id')->withTimestamps();
      }

      public function subjects() 
      {
        return $this->belongsToMany(Subject::class)->withPivot(['student_id', 'subject_id', 'attendance_id'])->withTimestamps();
      }

      protected $casts = [
        // 'student_id' => 'array',
    ];
}
