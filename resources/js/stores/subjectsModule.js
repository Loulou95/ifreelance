import {URLs} from '../helpers/URLs';


export const subjectsModule = {
  namespaced: true,
  
  state: {
    subject: {},
    subjects: []
  },
  mutations: {
    getSubjects (state, subjects) {
      state.subjects = subjects;
    },
    addSubject (state, subject) {
      state.subjects.push(subject);
    },
  },

  actions: {
    getSubjects (context) {
      axios.get(URLs.core.subjects.root).then(
          response => {
              context.commit('getSubjects', response.data)

          }
      );
  },

  addSubject (context, subject) {
    context.commit('addSubject', subject);
  },
}
}
