<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Undocumented class
 */
class AbstractRequest extends FormRequest
{
    /**
     * Returns an array of the variables in the request that are in the rules.
     *
     * @return Array
     */
    public function getRequestFields()
    {
        return $this->only(array_keys($this->rules()));
    }
}
