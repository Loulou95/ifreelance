<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Delegates\Contracts\ExamDelegateContract;
use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Http\Requests\Exam\ExamStoreRequest;
use App\Http\Requests\Exam\ExamUpdateRequest;
use App\Http\Requests\Exam\ExamDestroyRequest;
/**
 * Delegate Controller - Exam
 */
class ExamController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\ExamResource
     */
    public $resource = \App\Http\Resources\ExamResource::class;

    /**
     * The delegate constructor
     *
     * @param ExamDelegateContract $delegate The delegate for this controller
     */
    public function __construct(ExamDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\ExamStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ExamStoreRequest $request)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->save($request->getRequestFields())
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\ExamUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ExamUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
