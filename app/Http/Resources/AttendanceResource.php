<?php

namespace App\Http\Resources;

/**
 * API Resource - Attendance
 */
class AttendanceResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'student_id' => $this->student_id,
            'register' => $this->register,
        ];
    }
}
