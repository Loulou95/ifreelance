<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Delegates\Contracts\EventDelegateContract;
use App\Http\Requests\Event\EventDestroyRequest;
use App\Http\Requests\Event\EventUpdateRequest;
use App\Http\Requests\Event\EventStoreRequest;
use App\Mail\EventMail;
use Illuminate\Support\Facades\Mail;
use App\Event;

/**
 * Delegate Controller - Event
 */
class EventController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\EventResource
     */
    public $resource = \App\Http\Resources\EventResource::class;

    /**
     * The delegate constructor
     *
     * @param EventDelegateContract $delegate The delegate for this controller
     */
    public function __construct(EventDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\EventStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(EventStoreRequest $request)
    {
        $fields = $request->getRequestFields();
        $email_option = $fields['email_option'];
        $event =  $this->delegate->save($fields);
        if ($email_option == 'true') {
            Mail::to('louisversace@yahoo.com')->send(new EventMail($event));
        }
        return $this->response(
            $event
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\EventUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(EventUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
