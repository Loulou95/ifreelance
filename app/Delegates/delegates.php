<?php

return [
    'App\Delegates\Contracts\AssignmentsDelegateContract' => 'App\Delegates\AssignmentsDelegate',
    'App\Delegates\Contracts\AttendanceDelegateContract' => 'App\Delegates\AttendanceDelegate',
    'App\Delegates\Contracts\BehaviourDelegateContract' => 'App\Delegates\BehaviourDelegate',
    'App\Delegates\Contracts\EventDelegateContract' => 'App\Delegates\EventDelegate',
    'App\Delegates\Contracts\ExamDelegateContract' => 'App\Delegates\ExamDelegate',
    'App\Delegates\Contracts\GradeDelegateContract' => 'App\Delegates\GradeDelegate',
    'App\Delegates\Contracts\GuardianDelegateContract' => 'App\Delegates\GuardianDelegate',
    'App\Delegates\Contracts\StudentDelegateContract' => 'App\Delegates\StudentDelegate',
    'App\Delegates\Contracts\StudentDetailDelegateContract' => 'App\Delegates\StudentDetailDelegate',
    'App\Delegates\Contracts\StudentSubjectDelegateContract' => 'App\Delegates\StudentSubjectDelegate',
    'App\Delegates\Contracts\SubjectDelegateContract' => 'App\Delegates\SubjectDelegate',
    'App\Delegates\Contracts\TimetableDelegateContract' => 'App\Delegates\TimetableDelegate',
    'App\Delegates\Contracts\UserDelegateContract' => 'App\Delegates\UserDelegate',

];
