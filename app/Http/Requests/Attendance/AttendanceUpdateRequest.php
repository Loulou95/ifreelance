<?php

namespace App\Http\Requests\Attendance;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Attendance class
 */
class AttendanceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_id' => 'required|uuid|exists:subjects,id',
            'student_id' => 'required|uuid|exists:students,id',
            'register' => 'required|string|max:50',
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
