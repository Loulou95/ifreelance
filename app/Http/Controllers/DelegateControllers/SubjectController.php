<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Delegates\Contracts\SubjectDelegateContract;
use App\Http\Requests\Subject\SubjectDestroyRequest;
use App\Http\Requests\Student\StudentSubjectRequest;
use App\Http\Requests\Subject\SubjectUpdateRequest;
use App\Http\Requests\Subject\SubjectStoreRequest;

/**
 * Delegate Controller - Subject
 */
class SubjectController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\SubjectResource
     */
    public $resource = \App\Http\Resources\SubjectResource::class;

    /**
     * The delegate constructor
     *
     * @param SubjectDelegateContract $delegate The delegate for this controller
     */
    public function __construct(SubjectDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\SubjectStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectStoreRequest $request)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->save($request->getRequestFields())
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\SubjectUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SubjectUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
