<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Guardian;

class GuardianController extends Controller
{
    public function index(){
        return Guardian::latest()->paginate(10);
    }
}
