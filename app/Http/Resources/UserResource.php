<?php

namespace App\Http\Resources;

/**
 * API Resource - User
 */
class UserResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->last_name,
            'password' => $this->password,
        ];
    }
}
