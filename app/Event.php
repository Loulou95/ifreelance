<?php

namespace App;

use App\AbstractModel as Model;

class Event extends Model
{
    protected $fillable = [
        'event_name',
        'event_details',
        'event_date',
        'event_start_time',
        'event_end_time',
        'email_option'
    ];

    protected $casts = [
        'event_date' => 'datetime',
    ];

    protected $date = [
        'event_date',
    ];
}
