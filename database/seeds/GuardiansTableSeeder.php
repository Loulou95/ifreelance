<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class GuardianTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('guardians')->insert([
        'id'  => Uuid::generate(4),
        'student_id'  => Uuid::generate(4),
        'email' => 'jae@yahoo.com',
        'first_name' => 'Chris',
        'last_name' => 'George',
        'other_name' => 'N',
        'mobile' => '07847374776',
        'phone' => '07847374736',
        'email_verified_at' => Carbon::now(),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
    }
}
