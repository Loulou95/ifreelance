<?php

namespace App\Delegates;

use App\Exceptions;
use App\Exceptions\ApiModelNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;

/**
 * And root delgate
 */
abstract class AbstractDelegate
{
    /**
     * The model for this delegate
     *
     * @var
     */
    public $model;

    /**
     * The model name for this delegate.
     * Override in subclass to change the name.
     *
     * @var
     */
    public $modelName;

    /**
     * Update the model
     *
     * @param Array   $data The data for the update.
     * @param Integer $id   The id of the entity to update.
     *
     * @return void
     */
    public function amend($data, $id)
    {
        $model = $this->find($id);

        $modelData = $this->getModelFields($data);

        foreach (array_keys($modelData) as $column) {
            $model->$column = $data[$column];
        }

        $model->save();

        return $model;
    }

    /**
     * Amend a collection of the model
     * TODO: find a better way
     *
     * @param Array $data The data to save.
     *
     * @return void
     */
    public function amendMany($data)
    {
        $result = collect([]);

        foreach ($data as $datum) {
            $result->push($this->amend($datum, $datum['id']));
        }

        return $result;
    }

    /**
     * Creates an instance of the model
     *
     * @return void
     */
    public function createModel()
    {
        return new $this->model;
    }

    /**
     * Deletes a model by id
     *
     * @return void
     */
    public function destroy($id)
    {
        $this->model::destroy($id);
        return;
    }

    /**
     * Find a model by id.
     *
     * @param Integer $id The id to search for.
     *
     * @return void
     */
    public function find($id)
    {
        try {
            $result = $this->model::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new ApiModelNotFoundException($this->model);
        }

        return $result;
    }

    /**
     * Find a model for value by column.
     *
     * @param array|string $columns The columns to search by.
     * @param array|string $values  The values to search for.
     *
     * @return Collection
     */
    public function findBy($columns, $values)
    {
        $query = $this->model::query();

        if (is_array($columns)) {
            for ($i = 0, $j = count($columns); $i < $j; $i++) {
                $query->where($columns[$i], $values[$i]);
            }
        } else {
            $query->where($columns, $values);
        }

        return  $query->get();
    }

    /**
     * Returns only the key in $model->fillable.
     *
     * @param [type] $data The data.
     *
     */
    public function getModelFields($data)
    {
        $model = new $this->model;
        return Arr::only($data, $model->getFillable());
    }

    /**
     * Get all the models for this object
     *
     * @return Array
     */
    public function getAll()
    {
        return $this->model::all();
    }

    /**
     * Returns a query obect for this model
     *
     * TODO: return the right query for searching
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->model::select();
    }

    /**
     * Save the model
     *
     * @param Array $data The data to save.
     *
     * @return void
     */
    public function save($data)
    {
        $model = $this->model::create($this->getModelFields($data));
        return $model;
    }

    /**
     * Save a collectiom of the model
     * TODO: find a better way
     *
     * @param Array $data The data to save.
     *
     * @return void
     */
    public function saveMany($data)
    {
        $result = [];

        foreach ($data as $datum) {
            $result[] = $this->save($datum);
        }

        return collect($result);
    }

    /**
     * Search for a model using 'LIKE'
     *
     * @param array $searchValues Key-value pairs of [column-name, search-value]
     *
     * @return void
     */
    public function search(array $searchValues)
    {

    }
}
