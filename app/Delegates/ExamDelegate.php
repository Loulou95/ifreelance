<?php

namespace App\Delegates;

use App\Delegates\Contracts\ExamDelegateContract;

/**
 * ExamDelegate
 */
class ExamDelegate extends AbstractDelegate implements ExamDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Exam
     */
    public $model = \App\Exam::class;
}
