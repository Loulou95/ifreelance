<?php

namespace App\Delegates;

use App\Delegates\Contracts\StudentDetailDelegateContract;

/**
 * StudentDetailDelegate
 */
class StudentDetailDelegate extends AbstractDelegate implements StudentDetailDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\StudentDetail
     */
    public $model = \App\StudentDetail::class;
}
