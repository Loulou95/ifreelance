import {URLs} from '../helpers/URLs';


export const guardiansModule = {
  namespaced: true,
  
  state: {
    guardian: {},
    guardians: []
    
  },
  mutations: {
    getGuardians (state, guardians) {
      state.guardians = guardians;
    },
    addGuardian (state, guardian) {
      state.guardians.push(guardian);
    },
  },

  actions: {
    getGuardians (context) {
      axios.get(URLs.core.guardians.root).then(
          response => {
              context.commit('getGuardians', response.data)
          }
      );
  },

  addGuardians (context, guardian) {
    axios.post(URLs.core.guardians.root, guardian).then(
        response => {
            context.commit('addGuardian', response.data);
        }
    );
  }
}
}
