import {URLs} from '../helpers/URLs';


export const attendancesModule = {
  namespaced: true,
  
  state: {
    attendance: {},
    attendances: []
  },
  mutations: {
    getAttendances (state, attendances) {
      state.attendances = attendances;
    },
    addAttendance (state, attendance) {
      state.attendances.push(attendance);
    },
  },

  actions: {
    getAttendances (context) {
      axios.get(URLs.core.attendances.root).then(
          response => {
              context.commit('getAttendances', response.data)

          }
      );
  },

  addAttendance (context, attendance) {
    axios.post(URLs.core.attendances.root, attendance).then(
        response => {
            context.commit('addAttendance', response.data);
        }
    );
  }
}
}
