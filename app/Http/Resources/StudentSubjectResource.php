<?php

namespace App\Http\Resources;

/**
 * API Resource - StudentSubject
 */
class StudentSubjectResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'student_id' => $this->student_id,
            'subject_id' => $this->subject_id,
        ];
    }
}
