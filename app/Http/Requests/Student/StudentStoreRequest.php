<?php

namespace App\Http\Requests\Student;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Student class
 */
class StudentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'guardian_id' => 'sometimes|uuid',
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'other_name' => 'sometimes|string|max:30',
            'gender' => 'sometimes|string',
            'role_id' => 'uuid|exists:roles,id',
            'email'  => 'required|string|max:255',
            'password' => 'required|string|min:8',
            'date_of_birth' => 'sometimes|string|max:50',
            'address_one' => 'required|string|max:50',
            'address_two' => 'required|string|max:50',
            'address_three' => 'sometimes|string|max:50',
            'postcode' => 'required|string|max:8',
            'year' => 'sometimes|string',
            'set' => 'sometimes|string',
            'student_photo.*' => 'sometimes|mimes:jpg,jpeg,bmp,png|max:20000'
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
