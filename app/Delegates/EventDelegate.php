<?php

namespace App\Delegates;

use App\Delegates\Contracts\EventDelegateContract;

/**
 * EventDelegate
 */
class EventDelegate extends AbstractDelegate implements EventDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Event
     */
    public $model = \App\Event::class;
}
