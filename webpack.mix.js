const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/studentApp.js', 'public/js')
   .js('resources/js/adminApp.js', 'public/js')
   .js('resources/js/parentApp.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css').sourceMaps().version();
 
// ===== Logged out code ===== //
mix.js('resources/js/auth.js', 'public/js')
   .js('resources/js/auth-parent.js', 'public/js')
   .sass('resources/sass/auth.scss', 'public/css').sourceMaps().version();