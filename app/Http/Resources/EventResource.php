<?php

namespace App\Http\Resources;
use Carbon\Carbon;

/**
 * API Resource - Event
 */
class EventResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        $start = Carbon::parse($this->event_start_time)->format('H-i-s');
        $end = Carbon::parse($this->event_end_time)->format('H-i-s');
        return [
            'id' => $this->id,
            'title' => $this->event_name,
            'event_details' => $this->event_name,
            'date' => $this->event_date->format('Y-m-d'),
            'start_time' => $start,
            'event_end_time' => $end,
        ];
    }
}
