<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Delegates\Contracts\GradeDelegateContract;
use App\Http\Requests\Grade\GradeDestroyRequest;
use App\Http\Requests\Grade\GradeUpdateRequest;
use App\Http\Requests\Grade\GradeStoreRequest;
use App\Grade;

/**
 * Delegate Controller - Grade
 */
class GradeController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\GradeResource
     */
    public $resource = \App\Http\Resources\GradeResource::class;

    /**
     * The delegate constructor
     *
     * @param GradeDelegateContract $delegate The delegate for this controller
     */
    public function __construct(GradeDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\GradeStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(GradeStoreRequest $request)
    {
        $this->returnDatum(
            $this->delegate->save($request->getRequestFields())
        );

        $grades = Grade::where('grade_name', $request->grade_name)
            ->where('mark', $request->mark)
            ->where('student_id', $request->student_id)
            ->where('subject_id', $request->subject_id)->first();

        $grades->students()->attach(
            ['student_id' => $request->student_id], 
            ['subject_id' => $request->subject_id]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\GradeUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(GradeUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
