<?php

namespace App\Delegates;

use App\Delegates\Contracts\SubjectDelegateContract;

/**
 * SubjectDelegate
 */
class SubjectDelegate extends AbstractDelegate implements SubjectDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Subject
     */
    public $model = \App\Subject::class;
}
