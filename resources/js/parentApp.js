/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');


/* === Import Statements === */
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import Vuex from 'vuex';
import { routes } from './routes/parentRoutes'
import { URLs } from './helpers/URLs'
import { Rules } from './helpers/Rules'


/* === Use Statements === */
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(Vuetify)

/* === Mixins === */
Vue.mixin({
    data: function() {
      return {
        get URLs() {
          return URLs;
        },
        get Rules() {
          return Rules;
        }
      }
    }
  })

/* === Miscellaneous === */

const vuetify = new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#0686d8',
        secondary: '#47c7ec',
        accent: '#FDB72D',
      },
      dark: {
        primary: '#0686d8',
        secondary: '#47c7ec',
        accent: '#FDB72D',
      },
    },
    icons: {
      iconfont: 'md',
    },
  },
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./views/parent', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const store = new Vuex.Store({
    modules: {
        // agencies: agenciesModule,
    }
})


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    router,
    vuetify,
    store,
    el: '#app',
    created () {
        //
    }
});

