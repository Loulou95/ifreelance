<?php

namespace App;

use App\AbstractModel as Model;

class Exam extends Model
{
    protected $fillable = [
        'title',
        'exam_date',
        'start_time',
        'exam_room',
        'end_time',
        'duration',
        'details'
    ];

    protected $casts = [
        'exam_date' => 'datetime',
    ];

    protected $date = [
        'exam_date',
    ];

    public function subjects() {
        return $this->belongsTo(Subject::class);
      }
}
