<?php

namespace App\Delegates;

use App\Delegates\Contracts\GradeDelegateContract;

/**
 * GradeDelegate
 */
class GradeDelegate extends AbstractDelegate implements GradeDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Grade
     */
    public $model = \App\Grade::class;
}
