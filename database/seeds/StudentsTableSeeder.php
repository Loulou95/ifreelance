<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Faker\Factory as Faker;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('students')->insert(
        [
            'id' => Uuid::generate(4),
            'first_name' => 'Chris',
            'last_name' => 'bedder',
            'other_name' => 'bin',
            'gender' => 'male',
            'date_of_birth' => '07/08/1990',
            'address_one' => 'Humber Road',
            'address_two' => 'Cheris Lane',
            'address_three' => 'Brick Lane',
            'postcode' => 'P75 48L',
            'year' => '7',
            'set' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
      ]
        );
    }
}
