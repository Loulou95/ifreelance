<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'id' => Uuid::generate(4),
          'email' => 'jamee@yahoo.com',
          'name' => 'avid',
          'password' => Hash::make('password'),
          'email_verified_at' => Carbon::now(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),

        ]);
    }
}
