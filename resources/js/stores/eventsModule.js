import {URLs} from '../helpers/URLs';


export const eventsModule = {
  namespaced: true,
  
  state: {
    event: {},
    events: []
  },
  mutations: {
    getEvents (state, events) {
      state.events = events;
    },
    addEvent (state, event) {
      state.events.push(event);
    },
  },

  actions: {
    getEvents (context) {
      axios.get(URLs.core.events.root).then(
          response => {
              context.commit('getEvents', response.data)

          }
      );
  },

  addEvent (context, event) {
    axios.post(URLs.core.events.root, event).then(
        response => {
            context.commit('addEvent', response.data);
        }
    );
  }
},
  getters: {
    EVENTS: state => state.events
  },
}
