<?php

namespace App;

use App\AbstractModel as Model;

class Behaviour extends Model
{
    protected $fillable = [
        'subject_id',
        'student_id',
        'status',
        'date',
        'comment',
        'type',
        'action'
      ];

      public function students() {
        return $this->belongsToMany(Student::class);
      }

      public function subjects() {
        return $this->belongsToMany(Subject::class);
      }

    protected $casts = [
        'date' => 'datetime',
    ];

    protected $date = [
        'date',
    ];
}
