<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DelegateServiceProvider extends ServiceProvider
{
    /**
     * [register description]
     *
     * @return [type] [description]
     */
    public function register()
    {
        $delegates = include app_path('Delegates/delegates.php');

        foreach ($delegates as $contract => $delgate) {
            $this->app->bind($contract, $delgate);
        }
    }

    /**
     * [provides description]
     *
     * @return [type] [description]
     */
    public function provides()
    {
    }
}
