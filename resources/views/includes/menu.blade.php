<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
 <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .searcher {
       display: flex;
       /*justify-content: space-between;*/
       width: 70%;
       flex-direction: row;
       margin: 0 auto;
       flex-wrap: wrap;

     }




            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .title{
                color: black;
                font-size: 30px;
            }
            .input-box {
      height: 33px;
          width: 32%;
     margin: 0 auto;
    }
        </style>
    </head>

    <body>
        <div class="header">
        <div class="flex-center position-ref full-height">
           @if (Route::has('login'))
           <div class="top-right links">
             @auth
             <div class="account">
             <a href="" style="float:right; margin-right: 10px;   font-size: 12px; color: black; color: #307f9c;">Create Profile</a>
              <a href="" style="float:right; margin-right: 10px;   font-size: 12px; color: black; color: #307f9c;">Account</a>
          </div></br>
             <a class="dropdown-item" href="{{ route('logout') }}" style="float: right; margin-right: 10px;   font-size: 12px; color: black;" onclick="event.preventDefault();

                                          document.getElementById('logout-form').submit();">Logout</a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST">
               @csrf
             </form>

             @else

             <div style="float:right;">

               <a href="{{ route('login') }}">
                 <button>Login</button>
               </a>
               @if (Route::has('register'))

               <a href="{{ route('register') }}">
                 <button>Register</button>
               </a>
             </div>
             @endif
             @endauth
           </div>
           @endif
         </div>
        <h2 class="title"> Laravel </h2>

        <form method="get" type="submit">

      <div class="mobile-2">

        {{ csrf_field() }}
        <div class="searcher">

          <input type="text" class="input-box" name="search" value="{{$_GET['search'] ?? ''}}" placeholder="I'm looking for..." type="text">

          <input type="text" class="input-box" name="location" value="{{$_GET['location'] ?? ''}}" type="text" placeholder="Location">

          <button type="submit" style=" width: 87px; background-color: #307f9c; border: 1px solid transparent; color:white; height: 39px; margin-left: 10px;">Search</button>

        </div>


      </div>
  </div>
    </body>
</html>
