<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Delegates\Contracts\TimetableDelegateContract;
use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Http\Requests\Timetable\TimetableStoreRequest;
use App\Http\Requests\Timetable\TimetableUpdateRequest;
use App\Http\Requests\Timetable\TimetableDestroyRequest;
/**
 * Delegate Controller - Timetable
 */
class TimetableController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\TimetableResource
     */
    public $resource = \App\Http\Resources\TimetableResource::class;

    /**
     * The delegate constructor
     *
     * @param TimetableDelegateContract $delegate The delegate for this controller
     */
    public function __construct(TimetableDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\TimetableStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(TimetableStoreRequest $request)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->save($request->getRequestFields())
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\TimetableUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(TimetableUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
