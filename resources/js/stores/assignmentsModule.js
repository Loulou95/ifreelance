import {URLs} from '../helpers/URLs';


export const assignmentsModule = {
  namespaced: true,
  
  state: {
    assignments: {},
    assignmentss: []
  },
  mutations: {
    getAssignments (state, assignments) {
      state.assignments = assignments;
    },
    addAssignment (state, assignment) {
      state.assignments.push(assignment);
    },
  },

  actions: {
    getAssignments (context) {
      axios.get(URLs.core.assignments.root).then(
          response => {
              context.commit('getAssignments', response.data)

          }
      );
  },

  addAssignment (context, assignment) {
    axios.post(URLs.core.assignments.root, assignment).then(
        response => {
            context.commit('addAssignment', response.data);
        }
    );
  }
}
}
