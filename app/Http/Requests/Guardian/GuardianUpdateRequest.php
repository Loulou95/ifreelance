<?php

namespace App\Http\Requests\Guardian;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Guardian class
 */
class GuardianUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required|uuid|exists:students,id',
            'role_id' => 'uuid|exists:roles,id',
            'first_name' => 'required',
            'last_name' => 'required',
            'other_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'mobile' => 'required',
            'phone' =>'required',
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
