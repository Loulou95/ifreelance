import {URLs} from '../helpers/URLs';


export const usersModule = {
  namespaced: true,

      state: {
        user: {},
          users: [],
      },

      mutations: {
        deleteUser (state, user){
            state.users = _.remove(state.users, function (o) {
                return o.id !==user.id;
            });
        },
          getUsers (state, users) {
              state.users = users;
          },
          addUser (state, user) {
              state.users.push(user);
          },
      },

      actions: {
          getUsers (context) {
              axios.get(URLs.core.users.root).then(
                  response => {
                      context.commit('getUsers', response.data)
                  }
              );
          },
          addUser (context, user) {
              axios.post(URLs.core.users.root, user).then(
                  response => {
                      context.commit('addUser', response.data);
                  }
              );
          },

          deleteUser ({ commit }, user) {
              axios.delete(URLs.core.users.withId(user.id)).then(
                  response => {
                      commit('deleteUser', user);
                  }
              );
          },
      },

      getters: {} // Computed equivalent
}
