<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbstractModel extends Model
{
    use Traits\Uuids;

    public $incrementing = false;
    //
}
