<?php

namespace App\Http\Requests\Grade;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Grade class
 */
class GradeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'grade_name' => 'required|string',
            'mark' => 'required',
            'student_id' => 'required|uuid|exists:students,id',
            'subject_id' => 'required|uuid|exists:subjects,id',
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
