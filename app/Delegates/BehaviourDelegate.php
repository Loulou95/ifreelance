<?php

namespace App\Delegates;

use App\Delegates\Contracts\BehaviourDelegateContract;

/**
 * BehaviourDelegate
 */
class BehaviourDelegate extends AbstractDelegate implements BehaviourDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Behaviour
     */
    public $model = \App\Behaviour::class;
}
