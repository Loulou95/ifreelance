<?php

namespace App\Http\Resources;

/**
 * API Resource - Behaviour
 */
class BehaviourResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'student_id' => $this->student_id,
            'status' => $this->status,
            'date' => $this->date->format('Y-m-d'),
            'comment' => $this->comment,
            'type' => $this->type,
            'action' => $this->action,
        ];
    }
}
