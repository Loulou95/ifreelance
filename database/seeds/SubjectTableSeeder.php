<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert(
            [
                'id' => Uuid::generate(4),
                'subject_name' => 'English',
                'room_name' => 'Room1023',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => Uuid::generate(4),
                'subject_name' => 'French',
                'room_name' => 'Room103',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => Uuid::generate(4),
                'subject_name' => 'History',
                'room_name' => 'Room303',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => Uuid::generate(4),
                'subject_name' => 'Law',
                'room_name' => 'Room307',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );
    }
}
