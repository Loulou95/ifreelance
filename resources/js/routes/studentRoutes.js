import StudentView from '../views/student/StudentView';
import PageNotFoundView from '../views/PageNotFoundView';
import DashboardView from '../views/student/DashboardView';
import SubjectView from '../views/student/SubjectView';

export const routes = [
    {
        path: '/student/',
        name: 'student.home',
        component: StudentView,
        children: [
            {
                path: 'dashboard',
                name: 'student.dashboard',
                component: DashboardView
            },
            {
                path: 'subjects',
                name: 'SubjectView',
                component: SubjectView
            },
            {
                path: '*',
                name: '404',
                component: PageNotFoundView
            }
        ]
    },
];