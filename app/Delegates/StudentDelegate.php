<?php

namespace App\Delegates;

use App\Delegates\Contracts\StudentDelegateContract;

/**
 * StudentDelegate
 */
class StudentDelegate extends AbstractDelegate implements StudentDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Student
     */
    public $model = \App\Student::class;
}
