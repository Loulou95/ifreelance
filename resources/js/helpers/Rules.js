export const Rules = {
    required: [
        v => !!v || 'This field is required'
    ],
    email: [
        v => !!v || 'E-mail is required',
        v => /.+@.+/.test(v) || 'E-mail must be valid',
        v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed',
    ],
    name: [
        v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed',
    ],
    noSpaces: [
        v => !!v || 'Field is required',
        v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed',
    ],
    password: [
        v => !!v || 'A password is required',
        v => (v || '').length >= 8 || 'Password must be more than 8 characters'
    ],
    optionalPassword: [
        v => (v || '').length >= 8 || 'Password must be more than 8 characters'
    ],
    domain: [
        v => /^(((?!-))(xn--|_{1,1})?[a-z0-9-]{0,61}[a-z0-9]{1,1}\.)*(xn--)?([a-z0-9][a-z0-9\-]{0,60}|[a-z0-9-]{1,30}\.[a-z]{2,})$/.test(v) || 'Must have a valid domain name'
    ],
    positiveNumber: [
        v => (v > 0) || 'Field must be a positive number'
    ],
    phone: [
        v => !!v || 'Phone Number is required',
        v => /[0-9+]{11,13}/.test(v) || 'Between 11-13 digits required (+, 0-9)'
    ],
    summary: [
        v => !!v || 'A summary is required',
        v => (v || '').length <= 100 || 'Summary must be less than 100 characters'
    ],
    description: [
        v => !!v || 'A description is required',
        v => (v || '').length <= 1000 || 'Description must be less than 1000 characters'
    ],
    sortcode: [
        v => !!v || 'Sortcode is required',
        v => (v || '').length === 6 || 'Sort code must be 6 digits long',
        v => /[0-9]/.test(v) || 'Sort code must be numeric only',
        v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed',
    ],
    account_number: [
        v => !!v || 'Sort Code is required',
        v => (v || '').length === 8 || 'Sort code must be 8 digits long',
        v => /[0-9]/.test(v) || 'Account Number must be numeric only',
    ],
    bank_society_number: [
        v => !!v || 'Bank Society Number is required',
        v => (v || '').length === 8 || 'Sort code must be 8 digits long',
        v => /[0-9]/.test(v) || 'Bank Society Number must be numeric only',
        v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed',
    ],
    postcode: [
        v => !!v || 'Postcode is required',
        v => (v || '').length === 6 || 'Sort code must be 6 digits long',
        v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed',
    ],

    ni: [
        v => !!v || 'Postcode is required',
        v => (v || '').length === 9 || 'Sort code must be 9 digits long',
        v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed',
    ],
};
