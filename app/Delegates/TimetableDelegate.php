<?php

namespace App\Delegates;

use App\Delegates\Contracts\TimetableDelegateContract;

/**
 * TimetableDelegate
 */
class TimetableDelegate extends AbstractDelegate implements TimetableDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Timetable
     */
    public $model = \App\Timetable::class;
}
