<?php

namespace App\Http\Requests\Behaviour;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Behaviour class
 */
class BehaviourUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'subject_id' => 'required|uuid|exists:subjects,id',
            'student_id' => 'required|uuid|exists:students,id',
            'status' => 'required|string|max:50',
            'date' => 'required|date|max:50',
            'comment' => 'required|string|max:50',
            'type' => 'required|string|max:50',
            'action' => 'required|string|max:50',
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
