<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    use Traits\Uuids;
    
    public $incrementing = false;
    
    protected $table = 'students';

    protected $fillable = [
      'guardian_id',
      'first_name',
      'last_name',
      'other_name',
      'gender',
      'date_of_birth',
      'address_one',
      'address_two',
      'postcode',
      'year',
      'set',
      'role_id',
      'student_photo'
    ];

    protected $hidden = [
      'password', 'remember_token',
  ];

    public function guardian() {
      return $this->belongsTo(Guardian::class);
    }

    public function attendances() {
      return $this->belongsToMany(Attendance::class)->withTimestamps();
    }

    public function subjects() 
    {
      return $this->belongsToMany(Subject::class)->withTimestamps();
    }

    public function grades() {
      return $this->belongsToMany(Grade::class)->withTimestamps();
    }

    public function behaviours() {
      return $this->hasOne(Behaviour::class);
    }

    public function roles()
    {
        return $this->hasOne('App\Role');
    }
}