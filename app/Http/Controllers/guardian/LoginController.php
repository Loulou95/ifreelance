<?php

namespace App\Http\Controllers\Guardian;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/guardian/dashboard';

    public function login(Request $request)
    {
        if(Auth::guard('guardian')->attempt(
            $request->only('email', 'password'),
            // $request->filled('remember')
        )) {
            //Authentication passed...
            return response('Logged in successfully');
        }
    
        //Authentication failed...
        return $this->loginFailed();
    }

    private function loginFailed()
    {
        return response('Incorrect details', 400);
    }

    public function logout()
    {
        Auth::guard('guardian')->logout();
        return redirect()
            ->route('guardian-login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
