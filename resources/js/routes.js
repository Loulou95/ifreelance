
import DashboardView from './components/DashboardViewComponent';

import StudentIndexView from './core/views/students/StudentIndexView';
import CreateStudent from './core/views/students/CreateStudentView';
import StudentUpdateView from './core/views/students/StudentUpdateView';
import StudentShowView from './core/views/students/StudentShowView';

import GuardiansView from './core/views/guardians/GuardiansView';
import CreateGuardianView from './core/views/guardians/CreateGuardianView';
import CreateSubjectView from './core/views/subjects/CreateSubjectView';
import SubjectsView from './core/views/subjects/SubjectsView';
import UserView from './core/views/users/UserView';
import CreateUser from './core/views/users/CreateUserView';
import AttendanceView from './core/views/attendance/AttendanceView';
import AssignmentsView from './core/views/assignments/AssignmentsView';
import AssignmentsCreate from './core/views/assignments/AssignmentsCreate';
import EventView from './core/views/events/EventView';
import BehaviourView from './core/views/behaviours/BehaviourView';
import CreateBehaviourView from './core/views/behaviours/CreateBehaviourView';
import CreateEventView from './core/views/events/CreateEventView';
import TimetableView from './core/views/timetables/TimetableView';
import ExamsView from './core/views/exams/ExamsView';
import CreateExamView from './core/views/exams/CreateExamView';
import CreateTimetableView from './core/views/timetables/CreateTimetableView';
import PageNotFoundView from './views/PageNotFoundView';
import StudentView from './views/student/StudentView';
import ParentView from './views/parent/ParentView';
import ReportView from './views/parent/ReportView';
import Exams from './views/parent/Exams';
import DashboardViewTwo from './views/parent/DashboardViewTwo';

export const routes = [
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardView
    },
    {
      path: '/timetable',
      name: 'timetable',
      component: TimetableView
    },
    {
      path: '/timetable/create',
      name: 'timetable.create',
      component: CreateTimetableView
    },
    {
      path: '/students',
      name: 'StudentView',
      component: StudentIndexView
    },
    {
      path: '/exams',
      name: 'ExamView',
      component: ExamsView
    },
    {
      path: '/exams/create',
      name: 'exam.create',
      component: CreateExamView
    },
    {
      path: '/behaviour',
      name: 'BehaviourView',
      component: BehaviourView
    },
    {
      path: '/behaviour/create',
      name: 'CreateBehaviourView',
      component: CreateBehaviourView
    },
    {
      path: '/users',
      name: 'UserView',
      component: UserView
    },
    {
      path: '/subjects',
      name: 'SubjectsView',
      component: SubjectsView
    },
    {
      path: '/subjects/create',
      name: 'CreateSubjectView',
      component: CreateSubjectView
    },
    {
      path: '/guardians',
      name: 'GuardiansView',
      component: GuardiansView
    },
    {
      path: '/guardians/create',
      name: 'CreateGuardianView',
      component: CreateGuardianView
    },
    {
      path: '/students/create',
      name: 'CreateStudent',
      component: CreateStudent
    },
    {
      path: '/students/update/:id',
      name: 'StudentUpdateView',
      component: StudentUpdateView
    },
    {
      path: '/student/:id',
      name: 'student.show',
      component: StudentShowView
    },
    {
      path: '/users/create',
      name: 'CreateUser',
      component: CreateUser
    },
    {
      path: '/attendance',
      name: 'AttendanceView',
      component: AttendanceView
    },
    {
      path: '/assignments',
      name: 'assignments.view',
      component: AssignmentsView
    },
    {
      path: '/assignments/create',
      name: 'assignments.create',
      component: AssignmentsCreate
    },
    {
      path: '/events',
      name: 'events.view',
      component: EventView
    },
    {
      path: '/events/create',
      name: 'events.create',
      component: CreateEventView
    },
    {
      path: '/student/',
      name: 'student.home',
      component: StudentView,
      children: [
          {
              path: '*',
              name: '404',
              component: PageNotFoundView
          }
      ]
    },
    {
      path: '/guardian/',
      name: 'parent.home',
      component: ParentView,
      children: [
          {
              path: 'dashboard',
              name: 'parent.dashboard',
              component: DashboardViewTwo
          },
          {
              path: 'reports',
              name: 'parent.reports',
              component: ReportView
          },
          {
              path: 'exams',
              name: 'parent.exams',
              component: Exams
          },
          {
              path: '*',
              name: '404',
              component: PageNotFoundView
          }
          
      ]
    }
];
