<?php

namespace App\Http\Controllers\DelegateControllers;

use Illuminate\Routing\Controller;

/**
 * The root delegate controller
 */
class DelegateController extends Controller
{
    /**
     * The delegate for this controller.
     *
     * @var [type]
     */
    public $delegate;

    /**
     * The api resource for this object.
     * Override in subclass to NOT use default.
     *
     * Default: FooClass => FooResource
     *
     * TODO: implement the default behaviour
     *
     * @var App\Http\Resources
     */
    public $resource;

    /**
     * The 'return as resource' flag.
     *
     * @var boolean true - return data with the resource
     *              false - return data raw
     */
    public $withResource = true;

    /**
     * Undocumented function
     *
     * @param [type] $delegate The delegate for this controller.
     */
    public function __construct($delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param integer $id The id of the object to destroy
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->delegate->destroy($id);
        return $this->response(['message' => 'Deleted Successfully']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->response(
            $this->withResource()->returnData($this->delegate->getAll())
        );
    }

    /**
     * Undocumented function
     *
     * @param Array $data The data for the response
     *
     * @return string
     */
    public function response($data)
    {
        return response()->json($data);
    }

    /**
     * Return the data in a format.
     *
     * @param Array $data The data being returned
     *
     * @return Array
     */
    public function returnData($data)
    {
        if ($this->withResource) {
            $data = $this->resource::collection($data);
        }

        return $data;
    }

    /**
     * Return the data in a format.
     *
     * @param Array $data The data being returned
     *
     * @return Array
     */
    public function returnDatum($data)
    {
        if ($this->withResource) {
            $data = new $this->resource($data);
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param integer $id The id of the entity to show
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response(
            $this->withResource()->returnDatum($this->delegate->find($id))
        );
    }

    /**
     * Set the 'return as resource' flag.
     *
     * @param boolean $withResource true  - return data with the resource
     *                              false - return data raw
     *
     * @return this
     */
    public function withResource($withResource = true)
    {
        $this->withResource = $withResource;

        return $this;
    }
}
