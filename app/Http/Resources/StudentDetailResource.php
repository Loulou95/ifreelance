<?php

namespace App\Http\Resources;

/**
 * API Resource - StudentDetail
 */
class StudentDetailResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
        ];
    }
}
