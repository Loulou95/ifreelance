<?php

namespace App\Delegates;

use App\Delegates\Contracts\AssignmentsDelegateContract;

/**
 * AssignmentsDelegate
 */
class AssignmentsDelegate extends AbstractDelegate implements AssignmentsDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Assignments
     */
    public $model = \App\Assignments::class;
}
