<?php

namespace App\Http\Resources;

/**
 * API Resource - Guardian
 */
class GuardianResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'student_id' => $this->student_id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'other_name' => $this->other_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
        ];
    }
}
