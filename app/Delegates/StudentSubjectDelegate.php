<?php

namespace App\Delegates;

use App\Delegates\Contracts\StudentSubjectDelegateContract;

/**
 * StudentSubjectDelegate
 */
class StudentSubjectDelegate extends AbstractDelegate implements StudentSubjectDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\StudentSubject
     */
    public $model = \App\StudentSubject::class;
}
