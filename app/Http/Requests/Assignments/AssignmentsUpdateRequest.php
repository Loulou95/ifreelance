<?php

namespace App\Http\Requests\Assignments;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Assignments class
 */
class AssignmentsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'subject_id' => 'required|uuid|exists:subjects,id',
            'title' => 'required|string|max:100',
            'body' => 'required|string|max:250',
            'due_date' => 'required|date|max:50',
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
