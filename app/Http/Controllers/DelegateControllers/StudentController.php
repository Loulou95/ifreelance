<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Delegates\Contracts\StudentDelegateContract;
use App\Http\Requests\Student\StudentUpdateRequest;
use App\Http\Requests\Student\StudentStoreRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Student;



/**
 * Delegate Controller - Student
 */
class StudentController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\StudentResource
     */
    public $resource = \App\Http\Resources\StudentResource::class;

    /**
     * The delegate constructor
     *
     * @param StudentDelegateContract $delegate The delegate for this controller
     */
    public function __construct(StudentDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\StudentStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StudentStoreRequest $request)
    {
       
        $filename = Str::random(20);
        $path = Storage::disk('public')->put('pictures/' . $filename, $request->file('student_photo'));
        $fields = $request->getRequestFields();
        $fields['student_photo'] = $path;
        $fields['password'] = Hash::make($fields['password']);

        $student = $this->delegate->save($fields);
        return $this->response(
            $this->returnDatum(
                $student
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\StudentUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StudentUpdateRequest $request, $id)
    {
        try{
            $fields = $request->getRequestFields();
            $id = $request->id;
            return $this->response(
                $this->returnDatum(
                    $this->delegate->amend($fields, $id)
                )
            );
        } catch (\Exception $e){ return response(['error' => 'Failed tocreate Student'], 500);
        }
    }

    public function destroy($id)
    {
        $student = Student::where('id', $id);
        $student->delete();
    }
}
