import {URLs} from '../helpers/URLs';


export const gradesModule = {
  namespaced: true,
  
  state: {
    grade: {},
    grades: []
  },
  mutations: {
    getGrades (state, grades) {
      state.grades = grades;
    },
    addGrade (state, grade) {
      state.grades.push(grade);
    },
  },

  actions: {
    getGrades (context) {
      axios.get(URLs.core.grades.root).then(
          response => {
              context.commit('getGrades', response.data)

          }
      );
  },

    addGrade (context, grade) {
      axios.post(URLs.core.grades.root, grade).then(
          response => {
              context.commit('addGrade', response.data);
          }
      );
    }
  },
}
