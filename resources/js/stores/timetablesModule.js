import {URLs} from '../helpers/URLs';


export const timetablesModule = {
  namespaced: true,
  
  state: {
    timetable: {},
    timetables: []
  },
  mutations: {
    getTimetables (state, timetables) {
      state.timetables = timetables;
    },
    addTimetable (state, timetable) {
      state.timetables.push(timetable);
    },
  },

  actions: {
    getTimetables (context) {
      axios.get(URLs.core.timetables.root).then(
          response => {
              context.commit('getTimetables', response.data)

          }
      );
  },

  addTimetable (context, timetable) {
    context.commit('addTimetable', timetable);
  },
}
}
