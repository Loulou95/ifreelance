<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Delegates\Contracts\GuardianDelegateContract;
use App\Http\Requests\Guardian\GuardianUpdateRequest;
use App\Http\Requests\Guardian\GuardianStoreRequest;
use Illuminate\Support\Facades\Hash;
use App\Student;

/**
 * Delegate Controller - Guardian
 */
class GuardianController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\GuardianResource
     */
    public $resource = \App\Http\Resources\GuardianResource::class;

    /**
     * The delegate constructor
     *
     * @param GuardianDelegateContract $delegate The delegate for this controller
     */
    public function __construct(GuardianDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\GuardianStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(GuardianStoreRequest $request)
    {
        $fields = $request->getRequestFields();
        $fields['password'] = Hash::make($fields['password']);
        $guardian = $this->delegate->save($fields);

        Student::findOrFail($fields['student_id'])
            ->update(
                [
                    'guardian_id' => $guardian->id
                ]
            );
        return $this->response(
            $this->returnDatum(
                $guardian
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\GuardianUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(GuardianUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
