<?php

namespace App\Http\Resources;

/**
 * API Resource - Subject
 */
class SubjectResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject_name' => $this->subject_name,
            'room_name' => $this->room_name
        ];
    }
}
