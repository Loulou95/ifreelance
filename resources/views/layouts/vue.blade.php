<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Student Portal') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    @if (Auth::check() && Auth::user()->hasRole('teacher'))
        <script src="{{ asset('/js/app.js') }}" defer></script>
    @elseif (Auth::check() && Auth::user()->hasRole('admin'))
        <script src="{{ asset('/js/adminApp.js') }}" defer></script>
    @elseif(Auth::guard('student')->check())
        <script src="{{ asset('/js/studentApp.js') }}" defer></script>
    @elseif(Auth::guard('guardian')->check())
        <script src="{{ asset('/js/parentApp.js') }}" defer></script>
    @endif

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" data-app="true">
      <div id="app" data-app="true">
        @if (Auth::check() && Auth::user()->hasRole('teacher'))
        <app-view-component></app-view-component> 
        @elseif (Auth::check() && Auth::user()->hasRole('admin'))
            <admin-view></admin-view>
        @elseif(Auth::guard('student')->check())
            <student-view></student-view>
        @elseif(Auth::guard('guardian')->check())
            <parent-view></parent-view>
        @endif
    </div>
</body>
</html>
