<?php

namespace App\Http\Resources;

use App\Http\Resources\GuardianResource;
/**
 * API Resource - Student
 */
class StudentResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'full_name' => $this->first_name . ' ' . $this->last_name,
            'other_name' => $this->other_name,
            'gender' => $this->gender,
            'email' => $this->email,
            'date_of_birth' => $this->date_of_birth,
            'address_one' => $this->address_one,
            'address_two' => $this->address_two,
            'address_three' => $this->address_three,
            'postcode' => $this->postcode,
            'year' => $this->year,
            'set' => $this->set,
            'behaviour' => $this->behaviour,
            'guardian' => $this->guardian !== null ? new GuardianResource($this->guardian) : false,
            'behaviours' => $this->behaviours !== null ? new BehaviourResource($this->behaviours) : false,
            'student_photo' => $this->student_photo,
        ];
    }
}
