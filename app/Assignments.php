<?php

namespace App;

use App\AbstractModel as Model;

class Assignments extends Model
{
    protected $fillable = [
        'subject_id',
        'title',
        'body',
        'due_date',
      ];

      protected $date = [
        'due_date',
      ];
}
