<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Http\Requests\StudentSubject\StudentSubjectDestroyRequest;
use App\Http\Requests\StudentSubject\StudentSubjectUpdateRequest;
use App\Http\Requests\StudentSubject\StudentSubjectStoreRequest;
use App\Delegates\Contracts\StudentSubjectDelegateContract;
use Illuminate\Http\Request;
use App\StudentSubject;
use App\Subject;
/**
 * Delegate Controller - StudentSubject
 */
class StudentSubjectController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\StudentSubjectResource
     */
    public $resource = \App\Http\Resources\StudentSubjectResource::class;

    /**
     * The delegate constructor
     *
     * @param StudentSubjectDelegateContract $delegate The delegate for this controller
     */
    public function __construct(StudentSubjectDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\StudentSubjectStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StudentSubjectStoreRequest $request)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->save($request->getRequestFields())
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\StudentSubjectUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StudentSubjectUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }

    public function addToSubject(StudentSubjectStoreRequest $request)
    {
        $subject = Subject::where('id', $request->subject_id)->first();
        $exist = $subject->students()
            ->where('subject_id', $request->subject_id)
            ->where('student_id', $request->student_id)
            ->first();

        if ($exist) {
            return response(
                ['error' => "Student has already been added to class"], 500
            );
        } else {
            StudentSubject::create($request->except('_token'));
        }
    }

    public function getStudent(Request $request)
    {
        $subject = Subject::where('id', $request->subject_id)->first();
        $students = $subject->students;
        return $students;
    }
}
