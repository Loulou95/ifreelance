<?php

namespace App;

use App\AbstractModel as Model;

class Role extends Model
{
    protected $fillable = [
        'name'
    ];

    public function students() 
    {
        return $this->belongsToMany('App\Student');
    }

    public function guardians() 
    {
        return $this->belongsToMany('App\Guardian');
    }
    
    public function users() 
    {
        return $this->belongsToMany('App\User');
    }
}
