<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Role;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

$factory->define(Role::class, function (Faker $faker) {
    return [
        'id' => Uuid::generate(4),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
