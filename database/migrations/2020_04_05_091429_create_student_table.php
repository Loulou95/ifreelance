<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('address_one');
            $table->string('address_two');
            $table->string('postcode');
            $table->string('address_three')->nullable();
            $table->string('other_name')->nullable();
            $table->string('year')->nullable();
            $table->string('set')->nullable();
            $table->string('password');
            $table->string('email')->unique();
            $table->uuid('guardian_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
