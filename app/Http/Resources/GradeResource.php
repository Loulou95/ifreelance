<?php

namespace App\Http\Resources;

/**
 * API Resource - Grade
 */
class GradeResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'grade_name' => $this->grade_name,
            'mark' => $this->mark,
            'student_id' => $this->student_id,
            'subject_id' => $this->subject_id,
        ];
    }
}
