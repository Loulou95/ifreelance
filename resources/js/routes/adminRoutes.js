import DashboardView from '../components/DashboardViewComponent';
import AdminView from '../views/admin/AdminView';
import PageNotFoundView from '../views/PageNotFoundView';

export const routes = [
    {
        path: '/admin/',
        name: 'admin.home',
        component: AdminView,
        children: [
            {
                path: 'dashboard',
                name: 'admin.dashboard.view',
                component: DashboardView
            },
            {
                path: '*',
                name: '404',
                component: PageNotFoundView
            },
        ]
    }
];