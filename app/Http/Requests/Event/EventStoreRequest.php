<?php

namespace App\Http\Requests\Event;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Event class
 */
class EventStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_name' => 'required|string',
            'event_details' => 'required|string',
            'event_date' => 'required|date',
            'event_start_time' => 'required|date_format:hh:mm',
            'event_end_time' => 'required|date_format:hh:mm',
            'email_option' => 'boolean'
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
