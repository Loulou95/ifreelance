<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Delegates\Contracts\AttendanceDelegateContract;
use App\Http\Requests\Attendance\AttendanceStoreRequest;
use App\Http\Requests\Attendance\AttendanceUpdateRequest;
use App\Http\Requests\Attendance\AttendanceDestroyRequest;
use App\Http\Requests\Student\StudentYearRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Attendance;
use App\Subject;
use App\Student;

/**
 * Delegate Controller - Attendance
 */
class AttendanceController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\AttendanceResource
     */
    public $resource = \App\Http\Resources\AttendanceResource::class;

    /**
     * The delegate constructor
     *
     * @param AttendanceDelegateContract $delegate The delegate for this controller
     */
    public function __construct(AttendanceDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\AttendanceStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AttendanceStoreRequest $request)
    {
        
        return $this->response(
            $this->returnDatum(
                $this->delegate->save($request->getRequestFields())
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\AttendanceUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AttendanceUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }

    public function registerStudent(AttendanceStoreRequest $request) 
    {
        $attendance = Attendance::where('subject_id', $request->subject_id)
            ->where('student_id', $request->student_id)
            ->first();
        $exist = $attendance;
        if ($exist) {
            return response("Attendance has already been recorded for this student", 500);
        } else {
            Attendance::create($request->except('_token'));
            $attendance->students()->attach(
                ['student_id' => $request->student_id], 
                ['subject_id' => $request->subject_id]
            );
            $attendance->subjects()->attach(
                ['student_id' => $request->student_id], 
                ['subject_id' => $request->subject_id]
            );
        }
    }

    public function getAttendance(Request $request) 
    {
        $subject = Subject::where('id', '=', $request->subject_id)->first();
        $students = $subject->attendances;
        return $students;
    }

    public function getRegister(Request $request) 
    {
        $subject = Subject::where('id', '=', $request->subject_id)->first();
        $students = $subject->students;
        return $students;
    }
}
