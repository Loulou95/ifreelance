require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import FullCalendar from '@fullcalendar/vue'
import dayGridPlugin from '@fullcalendar/daygrid'

import 'vuetify/dist/vuetify.min.css'
import { routes } from './routes'
import { URLs } from './helpers/URLs'
import { Rules } from './helpers/Rules'
import { guardiansModule } from './stores/guardiansModule'
import { studentsModule } from './stores/studentsModule'
import { attendancesModule } from './stores/attendancesModule'
import { subjectsModule } from './stores/subjectsModule'
import { usersModule } from './stores/usersModule'
import { eventsModule } from './stores/eventsModule'
import { assignmentsModule } from './stores/assignmentsModule'
import { gradesModule } from './stores/gradesModule'
import { behavioursModule } from './stores/behavioursModule'
import AppViewComponent from './components/AppViewComponent'
import { timetablesModule } from './stores/timetablesModule';
import { examsModule } from './stores/examsModule';


Vue.use(Vuetify)
Vue.use(FullCalendar)
Vue.use(dayGridPlugin)
Vue.use(VueRouter)
Vue.use(Vuex)

Vue.mixin({
    data: function() {
      return {
        get URLs() {
          return URLs;
        },
        get Rules() {
          return Rules;
        }
      }
    }
  })

const vuetify = new Vuetify({
  theme:{
    themes:{
      light:{
        primary: '#0686d8',
        secondary: '#47c7ec',
        accent: '#FDB72D',
      },
    },
  },
});

const store = new Vuex.Store({
    modules: {
      attenendances: attendancesModule,
      behaviours: behavioursModule,
      assignments: assignmentsModule,
      events: eventsModule,
      exams: examsModule,
      grades: gradesModule,
      guardians: guardiansModule,
      students: studentsModule,
      subjects: subjectsModule,
      users: usersModule,
      timetable: timetablesModule
        
    }
})

const files = require.context('.', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/* eslint-disable no-new */
const router = new VueRouter({
  mode: 'history',
  routes: routes
});

const app = new Vue({
  router,
  store,
  vuetify,
  components:{
    AppViewComponent,
  },
  el: '#app',
  created (){
    if (_.isEmpty(this.$store.state.students.students) || this.$store.state.students.students === undefined) {
          this.$store.dispatch('students/getStudents');
    }
    if (_.isEmpty(this.$store.state.guardians.guardians) || this.$store.state.guardians.guardians === undefined) {
        this.$store.dispatch('guardians/getGuardians');
    }
    if (_.isEmpty(this.$store.state.subjects.subjects) || this.$store.state.subjects.subjects === undefined) {
        this.$store.dispatch('subjects/getSubjects');
    }
    if (_.isEmpty(this.$store.state.assignments.assignments) || this.$store.state.assignments.assignments === undefined) {
        this.$store.dispatch('assignments/getAssignments');
    }
    if (_.isEmpty(this.$store.state.events.events) || this.$store.state.events.events === undefined) {
        this.$store.dispatch('events/getEvents');
    }
    if (_.isEmpty(this.$store.state.exams.exams) || this.$store.state.exams.exams === undefined) {
        this.$store.dispatch('exams/getExams');
    }
  }

});
