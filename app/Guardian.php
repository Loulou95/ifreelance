<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Guardian extends Authenticatable
{
  use Traits\Uuids;

  public $incrementing = false;
  
  protected $table = 'guardians';

  protected $fillable = [
    'student_id',
    'role_id',
    'first_name',
    'last_name',
    'other_name',
    'email',
    'mobile',
    'phone',
  ];

  protected $hidden = [
    'password', 'remember_token',
];
  
  public function students() {
    return $this->hasMany(Student::class);
  }

  public function roles()
  {
      return $this->hasOne('App\Role');
  }

}
