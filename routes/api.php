<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    ['middleware' => ['auth']], function () {
        Route::name('api-')
            ->group(
                function () {
                    Route::namespace('DelegateControllers')->group(
                        function () {
                            Route::apiResources(
                                [
                                    'students' => 'StudentController',
                                    'guardians' => 'GuardianController',
                                    'users' => 'UserController',
                                    'subjects' => 'SubjectController',
                                    'attendances' => 'AttendanceController',
                                    'assignments' => 'AssignmentsController',
                                    'events' => 'EventController',
                                    'student-subjects' => 'StudentSubjectController',
                                    'grades' => 'GradeController',
                                    'behaviours' => 'BehaviourController',
                                    'exams' => 'ExamController'
                                ]
                            );
                            Route::post('/users/profile', [
                                'uses' => 'UserController@profile',
                            ]);

                            Route::post('/students/studentsClass', [
                                'uses' => 'StudentController@studentsClass',
                            ]);
                            Route::put('/students.update/{id}', [
                                'uses' => 'StudentController@update',
                                'as' => 'students.update'
                            ]);
                            Route::delete('/students/destroy/{id}', [
                                'uses' => 'StudentController@destroy',
                                'as' => 'students.destroy'
                            ]);

                            Route::post('/student-subjects/addToSubject', [
                                'uses' => 'StudentSubjectController@addToSubject',
                            ]);

                            Route::post('/student-subjects/getStudent', [
                                'uses' => 'StudentSubjectController@getStudent',
                            ]);

                            Route::post('/attendances/registerStudent', [
                                'uses' => 'AttendanceController@registerStudent',
                            ]);

                            Route::post('/attendances/getAttendance', [
                                'uses' => 'AttendanceController@getAttendance',
                            ]);

                            Route::post('/attendances/getRegister', [
                                'uses' => 'AttendanceController@getRegister',
                            ]);
                        }
                    );
                }
            );
    }
);
