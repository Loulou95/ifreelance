<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Delegates\Contracts\UserDelegateContract;
use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\User\UserDestroyRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
/**
 * Delegate Controller - User
 */
class UserController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\UserResource
     */
    public $resource = \App\Http\Resources\UserResource::class;

    /**
     * The delegate constructor
     *
     * @param UserDelegateContract $delegate The delegate for this controller
     */
    public function __construct(UserDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\UserStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        try{
            $fields = $request->getRequestFields();
            $fields['password'] = Hash::make($fields['password']);
            $user = $this->delegate->save($fields);
        } catch (\Exception $e) {
            return response(['error' => "Failed to create User"], 500);
        }
        $this->returnDatum(
            $user
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\UserUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }

    public function profile() 
    {
        $user = Auth::user();
        return $user;
    }
}
