<?php

namespace App\Http\Resources;

use Carbon\Carbon;
/**
 * API Resource - Assignments
 */
class AssignmentsResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        $date = Carbon::parse($request->due_date)->format('d-m-Y');
        return [
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'title' => $this->title,
            'body' => $this->body,
            'due_date' => $date,
        ];
    }
}
