<?php

namespace App\Http\Resources;
use Carbon\Carbon;

/**
 * API Resource - Exam
 */
class ExamResource extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return array
     */
    public function toArray($request)
    {
        $start = Carbon::parse($this->start_time)->format('H-i-s');
        $end = Carbon::parse($this->end_time)->format('H-i-s');
        return [
            'id' => $this->id,
            'title' => $this->title,
            'details' => $this->details,
            'exam_date' => $this->exam_date->format('Y-m-d'),
            'start_time' => $start,
            'end_time' => $end,
            'duration' => $this->duration,
            'exam_room' => $this->exam_room,
        ];
    }
}
