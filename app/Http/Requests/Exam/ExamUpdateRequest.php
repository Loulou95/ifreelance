<?php

namespace App\Http\Requests\Exam;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Exam class
 */
class ExamUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'title' => 'required',
            'details' => 'required|string',
            'exam_date' => 'required|date',
            'start_time' => 'required|date_format:hh:mm',
            'end_time' => 'required|date_format:hh:mm',
            'duration' => 'required|string',
            'exam_room' => 'required|string',
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
