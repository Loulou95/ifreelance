<?php

namespace App;

use App\AbstractModel as Model;

class Grade extends Model
{
    protected $fillable = [
        'grade_name',
        'mark',
        'student_id',
        'subject_id',
    ];

    public function students() {
        return $this->belongsToMany(Student::class)->withTimestamps();
    }

    public function subjects() {
        return $this->belongsTo(Subject::class);
    }
}
