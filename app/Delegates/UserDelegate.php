<?php

namespace App\Delegates;

use App\Delegates\Contracts\UserDelegateContract;

/**
 * UserDelegate
 */
class UserDelegate extends AbstractDelegate implements UserDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\User
     */
    public $model = \App\User::class;
}
