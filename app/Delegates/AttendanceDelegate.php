<?php

namespace App\Delegates;

use App\Delegates\Contracts\AttendanceDelegateContract;

/**
 * AttendanceDelegate
 */
class AttendanceDelegate extends AbstractDelegate implements AttendanceDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Attendance
     */
    public $model = \App\Attendance::class;
}
