<?php

namespace App\Http\Requests\Student;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * Student class
 */
class StudentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'role_id' => 'uuid|exists:roles,id',
            'gender' => 'sometimes|string',
            'email'  => 'required|string|max:255',
            'password' => 'required|string|min:8',
            'date_of_birth' => 'sometimes|string|max:50',
            'address_one' => 'sometimes|string|max:250',
            'address_two' => 'sometimes|string|max:250',
            'postcode' => 'sometimes|string|max:10',
            'year' => 'sometimes|string',
            'set' => 'sometimes|string',
            'student_photo' => 'sometimes|image|mimes:jpeg,png,jpg,gif|max:2048'
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
