<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('login', function () {
    return redirect('/');
});

Route::get('/self', function () {
    if (\Auth::check()) {
        return \Auth::user();
    } elseif (\Auth::guard('student')->check()) {
        return \Auth::guard('student')->user();
    } elseif (\Auth::guard('guardian')->check()) {
        return \Auth::guard('guardian')->user();
    }
});

Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('layouts.auth');
})->middleware(['guest']);

Auth::routes();

Route::namespace('Student')->name('student-')->prefix('student')->group(
    function () {
        // Login Routes
        Route::get('/login', function () {
            return view('layouts.student-auth');
        })->middleware(['guest']);
        Route::post('/login','LoginController@login')->name('login');
        Route::post('/logout','LoginController@logout')->name('logout');
        
        //Forgot Password Routes
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        
        //Reset Password Routes
        Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');
    }
);
Route::namespace('Guardian')->name('guardian-')->prefix('guardian')->group(
    function () {
        // Login Routes
        Route::get('/login', function () {
            return view('layouts.parent-auth');
        })->middleware(['guest']);
        Route::post('/login','LoginController@login')->name('login');
        Route::post('/logout','LoginController@logout')->name('logout');
        
        //Forgot Password Routes
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        
        //Reset Password Routes
        Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');
    }
);

Route::get('/{vue_capture?}', function () {
    return view('layouts.vue');
})->where('vue_capture', '[\/\w\.-]*');

