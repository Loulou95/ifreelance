<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Resource - Abstract
 * Root class for resource classes.
 */
class AbstractResource extends Resource
{
    /**
     * Undocumented variable
     *
     * @var array
     */
    public static $withs = null;

    /**
     * Undocumented function
     *
     * @param Request $request The request.
     *
     * @return array Collection of 'with' parts.
     */
    public function extractWiths($request)
    {
        $withs = [];

        if (self::$withs == null) {
            if ($request->has('with')) {
                $withs = explode(',', trim($request->input('with'), "[]"));
            }
            self::$withs = $withs;
        }


        return self::$withs;
    }

    /**
     * Undocumented function
     *
     * @param string $with the 'with' to reove
     *
     * @return void
     */
    public function remove($with)
    {
        self::$withs = array_values(array_diff(self::$withs, [$with]));

        // dd($withs);
    }
}
