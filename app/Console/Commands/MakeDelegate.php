<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * MakeDelegate artisan command
 */
class MakeDelegate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:delegate {name} 
                                {--full= : Whether to create models and migrations too}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates the files for a delegate';

    /**
     * The target folder for the controllers.
     *
     * @var [type]
     */
    protected $controllersTargetFolder;

    /**
     * The target folder for the contacts.
     *
     * @var [type]
     */
    protected $delegatesTargetFolder;

    /**
     * The target folder for the contacts.
     *
     * @var [type]
     */
    protected $requestsTargetFolder;

    /**
     * The target folder for the contacts.
     *
     * @var [type]
     */
    protected $resourceTargetFolder;

    /**
     * The source folder for the templates.
     *
     * @var [type]
     */
    protected $templatesSourceFolder;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->controllersTargetFolder = app_path('Http/Controllers/DelegateControllers');
        $this->delegatesTargetFolder = app_path('Delegates');
        $this->requestsTargetFolder = app_path('Http/Requests');
        $this->resourcesTargetFolder = app_path('Http/Resources');
        $this->testsTargetFolder = base_path('tests/Unit');
        $this->templatesSourceFolder = storage_path('delegate-templates');
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        if ($this->hasOption('full')) {
            $this->makeModel($name);
            $this->info("- $name Model created!");
            $this->info("- $name Migration created!");
        }

        $this->makeContract($name);
        $this->info("- $name Resource created!");
        
        $this->makeController($name);
        $this->info("- $name Requests created!");
        
        $this->makeDelegate($name);
        $this->info("- $name Test created!");
        
        $this->makeRequests($name);
        $this->info("- $name Contract created!");
        
        $this->makeResource($name);
        $this->info("- $name Controller created!");
        
        $this->makeTest($name);
        $this->info("- $name Delegate created!");
        
        $this->updateDelegatesFile($name);
        $this->info("- Delegate file updated!");

    }

    /**
     * Make the contract interface
     *
     * @param String $name the nmame for the contract
     *
     * @return void
     */
    public function makeContract($name)
    {
        $template = file_get_contents($this->templatesSourceFolder . '/contract.txt');

        file_put_contents(
            sprintf('%s/Contracts/%sDelegateContract.php', $this->delegatesTargetFolder, $name),
            $this->updateTemplate($name, $template)
        );
    }

    /**
     * Make the delegate controller class
     *
     * @param String $name the name of the delegate
     *
     * @return void
     */
    public function makeController($name)
    {
        $template = file_get_contents($this->templatesSourceFolder . '/controller.txt');

        file_put_contents(
            sprintf('%s/%sController.php', $this->controllersTargetFolder, $name),
            $this->updateTemplate($name, $template)
        );
    }

    /**
     * Make the delgate controller class
     *
     * @param String $name the name of the delegate
     *
     * @return void
     */
    public function makeDelegate($name)
    {
        $template = file_get_contents($this->templatesSourceFolder . '/delegate.txt');

        file_put_contents(
            sprintf('%s/%sDelegate.php', $this->delegatesTargetFolder, $name),
            $this->updateTemplate($name, $template)
        );
    }

    /**
     * Make the store and update request classes
     *
     * @param String $name the name of the delegate
     *
     * @return void
     */
    public function makeRequests($name)
    {

        mkdir(
            sprintf('%s/%s', $this->requestsTargetFolder, $name),
            '0777',
            true
        );

        $template = file_get_contents(
            $this->templatesSourceFolder . '/update-request.txt'
        );

        file_put_contents(
            sprintf('%s/%s/%sUpdateRequest.php', $this->requestsTargetFolder, $name, $name),
            $this->updateTemplate($name, $template)
        );

        $template = file_get_contents(
            $this->templatesSourceFolder . '/store-request.txt'
        );

        file_put_contents(
            sprintf('%s/%s/%sStoreRequest.php', $this->requestsTargetFolder, $name, $name),
            $this->updateTemplate($name, $template)
        );

        $template = file_get_contents(
            $this->templatesSourceFolder . '/destroy-request.txt'
        );

        file_put_contents(
            sprintf('%s/%s/%sDestroyRequest.php', $this->requestsTargetFolder, $name, $name),
            $this->updateTemplate($name, $template)
        );
    }

    /**
     * Make the resources classes
     *
     * @param string $name The name of the resource
     *
     * @return void
     */
    public function makeResource($name)
    {
        $template = file_get_contents($this->templatesSourceFolder . '/resource.txt');

        if (!is_dir($this->resourcesTargetFolder)) {
            mkdir($this->resourcesTargetFolder);
        }

        file_put_contents(
            sprintf('%s/%sResource.php', $this->resourcesTargetFolder, $name),
            $this->updateTemplate($name, $template)
        );
    }

    /**
     * Make the test class
     *
     * @param string $name The name of the delegate
     *
     * @return void
     */
    public function makeTest($name)
    {
        mkdir(
            sprintf('%s/%s', $this->testsTargetFolder, $name),
            '0777',
            true
        );

        $template = file_get_contents($this->templatesSourceFolder . '/unit-test.txt');

        file_put_contents(
            sprintf('%s/%s/%sTest.php', $this->testsTargetFolder, $name, $name),
            $this->updateTemplate($name, $template)
        );
    }

    /**
     * Update the delegates file.
     *
     * @param String $name the name of the delegate
     *
     * @return void
     */
    public function updateDelegatesFile($name)
    {
        $delegates = include app_path('Delegates/delegates.php');

        $contractName = sprintf('App\Delegates\Contracts\%sDelegateContract', $name);
        $delegateName = sprintf('App\Delegates\%sDelegate', $name);

        $delegates[$contractName] = $delegateName;

        asort($delegates);

        $template = file_get_contents(
            sprintf('%s/delegates-file.txt', $this->templatesSourceFolder)
        );


        $delegatesList = '';

        foreach ($delegates as $contract => $delegate) {
            $delegatesList .= sprintf(
                '    \'%s\' => \'%s\',%s',
                $contract,
                $delegate,
                PHP_EOL
            );
        };

        file_put_contents(
            sprintf('%s/delegates.php', $this->delegatesTargetFolder),
            str_replace('__delegate_contracts__', $delegatesList, $template)
        );
    }

    /**
     * Make the Model class
     *
     * @param string $name The name of the delegate
     *
     * @return void
     */
    public function makeModel($name)
    {
        $this->callSilent(
            'make:model',
            [
                'name'        => $name,
                '--migration' => true
            ]
        );
    }

    /**
     * Updates the text in the file with the name
     *
     * @param String $name     the name of the delegate.
     * @param String $template the source text for the file.
     *
     * @return void
     */
    public function updateTemplate($name, $template)
    {
        return str_replace('__name__', $name, $template);
    }
}
