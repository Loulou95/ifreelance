import {URLs} from '../helpers/URLs';


export const behavioursModule = {
  namespaced: true,
  
  state: {
    behaviour: {},
    behaviours: []
  },
  mutations: {
    getBehaviours (state, behaviours) {
      state.behaviours = behaviours;
    },
    addBehaviour (state, behaviour) {
      state.behaviours.push(behaviour);
    },
  },

  actions: {
    getBehviours (context) {
      axios.get(URLs.core.behaviours.root).then(
          response => {
              context.commit('getBehaviours', response.data)

          }
      );
  },

  addBehaviour (context, behaviour) {
    context.commit('addBehaviour', behaviour);
  },
}
}
