import coreRoutes  from '../urls/core';

export const URLs = {
    ...coreRoutes,
};
