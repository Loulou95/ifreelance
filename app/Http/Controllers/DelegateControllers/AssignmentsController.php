<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Delegates\Contracts\AssignmentsDelegateContract;
use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Http\Requests\Assignments\AssignmentsStoreRequest;
use App\Http\Requests\Assignments\AssignmentsUpdateRequest;
use App\Http\Requests\Assignments\AssignmentsDestroyRequest;
/**
 * Delegate Controller - Assignments
 */
class AssignmentsController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\AssignmentsResource
     */
    public $resource = \App\Http\Resources\AssignmentsResource::class;

    /**
     * The delegate constructor
     *
     * @param AssignmentsDelegateContract $delegate The delegate for this controller
     */
    public function __construct(AssignmentsDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\AssignmentsStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AssignmentsStoreRequest $request)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->save($request->getRequestFields())
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\AssignmentsUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AssignmentsUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
