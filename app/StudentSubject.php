<?php

namespace App;

use App\AbstractModel as Model;

class StudentSubject extends Model
{
    protected $table = 'student_subject';

    protected $fillable = [
        'student_id',
        'subject_id'
    ];

}
