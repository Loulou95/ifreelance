<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Delegates\Contracts\BehaviourDelegateContract;
use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Http\Requests\Behaviour\BehaviourStoreRequest;
use App\Http\Requests\Behaviour\BehaviourUpdateRequest;
use App\Http\Requests\Behaviour\BehaviourDestroyRequest;
use App\Student;
/**
 * Delegate Controller - Behaviour
 */
class BehaviourController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\BehaviourResource
     */
    public $resource = \App\Http\Resources\BehaviourResource::class;

    /**
     * The delegate constructor
     *
     * @param BehaviourDelegateContract $delegate The delegate for this controller
     */
    public function __construct(BehaviourDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\BehaviourStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BehaviourStoreRequest $request)
    {   
        $fields = $request->getRequestFields();
        $student = $this->delegate->save($fields);
        $behaviour = Student::where('id', $fields['student_id'])->first();

        if ($fields['status'] == 'Positive') {   
            $behaviour->increment('behaviour');
        } elseif ($fields['status'] == 'Negative') {
            $behaviour->decrement('behaviour');
        }
        return $this->response(
            $this->returnDatum(
                $student
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\BehaviourUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(BehaviourUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
