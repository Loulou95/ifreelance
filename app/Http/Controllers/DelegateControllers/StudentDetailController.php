<?php

namespace App\Http\Controllers\DelegateControllers;

use App\Delegates\Contracts\StudentDetailDelegateContract;
use App\Http\Controllers\DelegateControllers\DelegateController as Controller;
use App\Http\Requests\StudentDetail\StudentDetailStoreRequest;
use App\Http\Requests\StudentDetail\StudentDetailUpdateRequest;
use App\Http\Requests\StudentDetail\StudentDetailDestroyRequest;
/**
 * Delegate Controller - StudentDetail
 */
class StudentDetailController extends Controller
{
    /**
     * The resource for this delegate
     *
     * @var \App\Http\Resources\StudentDetailResource
     */
    public $resource = \App\Http\Resources\StudentDetailResource::class;

    /**
     * The delegate constructor
     *
     * @param StudentDetailDelegateContract $delegate The delegate for this controller
     */
    public function __construct(StudentDetailDelegateContract $delegate)
    {
        $this->delegate = $delegate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\StudentDetailStoreRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StudentDetailStoreRequest $request)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->save($request->getRequestFields())
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\StudentDetailUpdateRequest $request The request
     * @param Integer                  $id      The id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StudentDetailUpdateRequest $request, $id)
    {
        return $this->response(
            $this->returnDatum(
                $this->delegate->amend($request->all(), $id)
            )
        );
    }
}
