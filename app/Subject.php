<?php

namespace App;

use App\AbstractModel as Model;

class Subject extends Model
{
    protected $fillable = [
        'subject_name',
        'room_name'
      ];

    public function students() 
    {
      return $this->belongsToMany(Student::class)->withTimestamps();
    }

    public function attendances() 
    {
      return $this->belongsToMany(Attendance::class)->withTimestamps();
    }

    public function grades() 
    {
      return $this->hasOne(Grade::class);
    }

    public function behaviours() {
      return $this->hasOne(Behaviour::class);
    }

    public function exams() {
      return $this->hasMany(Exam::class);
    }
}
