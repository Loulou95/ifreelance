<?php

namespace App\Delegates;

use App\Delegates\Contracts\GuardianDelegateContract;

/**
 * GuardianDelegate
 */
class GuardianDelegate extends AbstractDelegate implements GuardianDelegateContract
{
    /**
     * The model for this delegate
     *
     * @var \App\Models\Guardian
     */
    public $model = \App\Guardian::class;
}
