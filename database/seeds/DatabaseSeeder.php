<?php

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
  public function run()
  {
      $this->call(
          [
            UsersTableSeeder::class,
            StudentsTableSeeder::class,
          ]
      );

    $roleNames = ['teacher', 'admin', 'student', 'guardian'];
    $roles = [];

    foreach ($roleNames as $role) {
        $roles[$role] = factory(App\Role::class, 1)->create(['name'=> $role])[0]->id;
    }
    
    $faker = Faker::create();
    $guardian = factory(App\Guardian::class, 10)->create()->each(function ($guardian) use ($faker, $roles) {

          for ($i = 0; 10 > $i; $i++){
            $gender = $faker->randomElement(['male', 'female']);

            factory(App\Guardian::class, 1)->create([
            'id' => Uuid::generate(4),
            'student_id'=> Uuid::generate(4),
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'role_id' => $roles['guardian'],
            'other_name' => (rand(0, 1) ? $faker->firstName : ''),
            'mobile' => $faker->phoneNumber,
            'phone' => (rand(0, 1) ? $faker->phoneNumber : ''),
            'email' => $faker->email,
            'password' => Hash::make('password123'),
            'email_verified_at' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ])->each(function($guardian) use ($faker, $gender, $roles) {
              factory(App\Student::class, 1)->create(
                [
                  'id' => Uuid::generate(4),
                  'first_name' => $faker->firstName,
                  'role_id' => $roles['student'],
                  'last_name' => $faker->lastName,
                  'other_name' => (rand(0, 1) ? $faker->firstName : ''),
                  'guardian_id' => $guardian->id,
                  'gender' => $gender,
                  'email' => $faker->unique()->safeEmail,
                  'password' => Hash::make('password123'),
                  'date_of_birth' => $faker->date($format = 'Y-m-d', $max = 'now'),
                  'address_one' => $faker->address,
                  'address_two' => $faker->address,
                  'address_three' => (rand(0, 1) ? $faker->address : ''),
                  'postcode' => $faker->postcode,
                  'year' => $faker->randomNumber(1),
                  'set' => $faker->randomNumber(1),
                  'created_at' => Carbon::now(),
                  'updated_at' => Carbon::now(),
              ]);
            });
          }
        });
      factory(App\User::class, 1)->create([
        'id' => Uuid::generate(4),
        'role_id' => $roles['teacher'],
        'name' => 'Louis',
        'email' => 'louis@yahoo.com',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('password12'),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ]);
      factory(App\User::class, 1)->create([
        'id' => Uuid::generate(4),
        'role_id' => $roles['admin'],
        'name' => 'Egbe',
        'email' => 'egbe@yahoo.com',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('password12'),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);
      factory(App\Event::class, 10)->create([
        'id' => Uuid::generate(4),
        'event_name' => $faker->name,
        'event_details' => $faker->text(),
        'event_date' => $faker->dateTimeBetween('1990-01-01', '2020-12-31')->format('Y-m-d H:i:s'),
        'event_start_time' => $faker->dateTime()->format('H:i:s'),
        'event_end_time' => $faker->dateTime()->format('H:i:s'),
        'email_option' => $faker->boolean(),
      ]);
  }
}