import DashboardViewTwo from '../views/parent/DashboardViewTwo';
import ParentView from '../views/parent/ParentView';
import ReportView from '../views/parent/ReportView';
import Exams from '../views/parent/Exams';
import PageNotFoundView from '../views/PageNotFoundView';

export const routes = [
    {
        path: '/guardian/',
        name: 'parent.home',
        component: ParentView,
        children: [
            {
                path: 'dashboard',
                name: 'parent.dashboard',
                component: DashboardViewTwo
            },
            {
                path: 'reports',
                name: 'parent.reports',
                component: ReportView
            },
            {
                path: 'exams',
                name: 'parent.exams',
                component: Exams
            },
            {
                path: '*',
                name: '404',
                component: PageNotFoundView
            }
            
        ]
    },
];