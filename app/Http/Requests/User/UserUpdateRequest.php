<?php

namespace App\Http\Requests\User;

use App\Http\Requests\AbstractRequest as FormRequest;

/**
 * User class
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'name' => 'required|string|max:30',
            'email' => 'required|string|max:30',
            'password' => 'required|string|max:30',
        ];
    }

    /**
     * Get the custom validation messages that apply to the rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
