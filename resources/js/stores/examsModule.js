import {URLs} from '../helpers/URLs';


export const examsModule = {
  namespaced: true,
  
  state: {
    exam: {},
    exams: []
  },
  mutations: {
    getExams (state, exams) {
      state.exams = exams;
    },
    addExam (state, exam) {
      state.exams.push(exam);
    },
  },

  actions: {
    getExams (context) {
      axios.get(URLs.core.exams.root).then(
          response => {
              context.commit('getExams', response.data)

          }
      );
  },

  addExam (context, exam) {
    axios.post(URLs.core.exams.root, exam).then(
        response => {
            context.commit('addExam', response.data);
        }
    );
  }
},
}
