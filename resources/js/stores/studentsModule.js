import {URLs} from '../helpers/URLs';


export const studentsModule = {
  namespaced: true,

      state: {
        student: {},
          students: [],
      },

      mutations: {
        deleteStudent (state, student){
            state.students = _.remove(state.students, function (o) {
                return o.id !==student.id;
            });
        },
          getStudents (state, students) {
              state.students = students;
          },
          addStudent (state, student) {
              state.students.push(student);
          },
          updateStudent (state, student) {
            state.students.push(student);
        },
      },

      actions: {
          getStudents (context) {
              axios.get(URLs.core.students.root).then(
                  response => {
                      context.commit('getStudents', response.data)
                  }
              );
          },
          addStudent (context, student) {
              axios.post(URLs.core.students.root, student).then(
                  response => {
                      context.commit('addStudent', response.data);
                  }
              );
          },

          updateStudent (context, student) {
            axios.post(URLs.core.students.withId(student.id), student).then(
                response => {
                    context.commit('newStudent', response.data);
                }
            );
        },

          deleteStudent ({ commit }, student) {
              axios.delete(URLs.core.students.withId(student.id)).then(
                  response => {
                      commit('deleteStudent', student);
                  }
              );
          },
      },

      getters: {} // Computed equivalent
}
